
// import BibleIcon from '../BibleIcon';

import {
	// TextControl,
	// Button,
	// Placeholder,
	DateTimePicker,
	Modal,
} from '@wordpress/components';

import {
	useBlockProps,
} from '@wordpress/block-editor';

import {
	useState,
} from '@wordpress/element';

import {
	useDispatch,
	useSelect,
} from '@wordpress/data';


export default function edit(props) {
	const blockProps = useBlockProps({
		className: "sermon-date-console"
	});


	const [isSelectingDate, setIsSelectingDate] = useState(0);

	const postDate = useSelect( select => select('core/editor').getCurrentPostAttribute('date') );
	const postTitle = useSelect( select => select('core/editor').getCurrentPostAttribute('title') );
	const postStatus  = useSelect( select => select('core/editor').getCurrentPostAttribute('status') );
	// const hasUnsavedEdits = useSelect( select => select('core/editor').isEditedPostDirty() )
	
	const getLastSunday = () => {
		var d = new Date();
		// get day of month, and subtract day of week int. Since Sunday is 0, this works easily
		d.setDate( d.getDate() - d.getDay() );
		return d;
	};
	
	// postDate is always populated, even on brand new post, this ternary isn't needed really
	var d = (postDate) ? new Date( postDate ) : Date();

	// const postId = useSelect( (select) => select('core/editor').getCurrentPostId() );

	const { editPost, savePost } = useDispatch('core/editor');
	const { saveEditedEntityRecord } = useDispatch('core');

	// const { isSelected } = props;

	if( postStatus == 'auto-draft' ) {
		d = getLastSunday();
		editPost({
			date: d,
		});
	}

	const setDate = function( date ) {
		editPost({ 
			// save the sermon date as the post date
			date: date,
		});
		savePost().then( () => {
			// need to ensure saving the post didn't override our custom date
			// This happens upon the very first publish of the sermon, if the
			// date isn't set to the most recent Sunday.
			// See #36
			editPost({
				date: date
			})
		});
		setIsSelectingDate(0);
	};

	const popupDateSelector = (isSelectingDate) ? (
		<Modal
			title="Choose Sermon Date"
			onRequestClose={ () => setIsSelectingDate(0) }
		>
			<DateTimePicker 
				onChange={ setDate }
				currentDate={ d }
			/>
		</Modal>
	) : false;

	var formattedDate = new Intl.DateTimeFormat('en-US', { dateStyle: 'full' } ).format( new Date(d) );

	return (
		<div {...blockProps}>

			{ popupDateSelector }

			<h4>
				<span>Sermon Date &mdash; </span> 
				<a href="#" onClick={ ()=>setIsSelectingDate(1) }>
					{ formattedDate }
				</a>
			</h4>

		</div>
	);
};
