<?php
defined('ABSPATH') || die('Not allowed');

add_action('init', function(){

	wp_enqueue_style( 'sermon-interface/blocks',
		SERMON_MANAGER_INTERFACE_URL . 'editor/build/style-index.css',
		[
		],
		SERMON_MANAGER_INTERFACE_VERSION
	);

	// register_block_type( 'sermon-interface/bible-verse', [
	// 	'render_callback' => function($atts, $content) {

	// 		$post_id = get_the_ID();
	// 		$s = new Sermon($post_id);

	// 		$book = $s->getBook();
	// 		// if( $book ){
	// 		// 	$bookLink = SermonsManager::getLinkToBibleBook($book);
	// 		// 	$flag = '####';
	// 		// 	$content = str_replace($flag, $bookLink, $content);
	// 		// }

	// 		// @TODO this goes somewhere else
	// 		// remove first audio block
	// 		// if( $s->hasBlock('core/audio') ) {
	// 		// 	$blocks = parse_blocks( $s->getDescription(true) );
	// 		// 	foreach($blocks as $index=>$block){
	// 		// 		if($block['blockName'] === 'core/audio' ){
	// 		// 			unset( $blocks[$index] );
	// 		// 			break;
	// 		// 		}
	// 		// 	}
	// 		// 	$content = serialize_blocks( $blocks );
	// 		// }

	// 		return $content;
	// 	},
	// ]);

});


add_action('enqueue_block_editor_assets', function(){

	wp_enqueue_style( 'sermon-interface/editor',
		SERMON_MANAGER_INTERFACE_URL . 'editor/build/index.css',
		[
		],
		SERMON_MANAGER_INTERFACE_VERSION
	);
	
	wp_enqueue_style( 'sermon-interface/blocks',
		SERMON_MANAGER_INTERFACE_URL . 'editor/build/style-index.css',
		[
		],
		SERMON_MANAGER_INTERFACE_VERSION
	);

	wp_enqueue_script( 'sermon-interface/editor',
		SERMON_MANAGER_INTERFACE_URL . 'editor/build/index.js',
		[
		],
		SERMON_MANAGER_INTERFACE_VERSION
	);

});

// add_action('after_setup_theme', function(){
// }, 20);