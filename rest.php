<?php
defined('ABSPATH') || die;

add_action('rest_api_init', function(){

	$routes = [];
	$defaults = [
		'methods' => ['POST'],
		// set permission_needed to false for a publicly viewable endpoint
		'permission_needed' => 'edit_posts',
	];

	$routes['/passage/'] = array_merge($defaults, [
		'callback' => 'sermonInterfaceRestGetPassage',
	]);


	foreach($routes as $endpoint => $args) {
		if( $args['permission_needed'] !== false ) {
			$args['permission_callback'] = function() use ($args) {
				return current_user_can( $args['permission_needed'] );
			};
		}
		register_rest_route('sermon-interface/v1', $endpoint, $args );
	}
});

function sermonInterfaceRestGetPassage($data) {

	if( ! isset($data['passage']) )
		return'No passage supplied';

	$args = [];
	if( isset($data['withStyles']) )
		$args['include-css-link'] = ($data['withStyles'])?'true':'false';

	return SermonsManager::getBibleTextFromSource( $data['passage'], $args);

}
