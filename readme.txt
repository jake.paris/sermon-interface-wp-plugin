=== Sermon Interface ===
Contributors: jakeparis
Donate link: https://jakeparis.com/
Requires at least: 5.0
Tested up to: 6.1.1
Stable tag: 4.0.0
Requires PHP: 5.4

Allow the management and presentation of sermons in the WP environment. It comes with some ready-to-use shortcodes to make Sermon integration pretty easy into any theme.


== Description ==

This is a Wordpress plugin to allow the management and presentation of sermons in the WP environment. It comes with some ready-to-use shortcodes to make Sermon integration pretty easy into any theme.

A unique feature of this plugin is integration with the ESV Online. Upon saving a sermon, the full bible text of the passage is automatically fetched and stored along with the sermon in the database. The passage can then be displayed to users along with each sermon.

You can create sermons just like creating a page or post.


== Available Shortcodes ==

= Display a Single Sermon =

Show a single sermon along with all of it's meta data, and any attached sound files. `[display-sermon]`. You can either give it the sermon title or the id.

```
[display-sermon id="123"]
// or
[display-sermon id="The Title of the Thing"]
```

= Sermon Page =

Show all sermons in a listing ordered newest first, and paginated. Optionally including search features against title, speaker, series, and bible book.

```
[sermons]
// or
[sermons with-search-box="false"]
```

= Latest Sermon Box =

Display an automatically-updating block with the latest sermon and it's meta-information. Optionally display any vido embedded in the sermon post content.

```
[latest-sermon]
// or
[latest-sermon with-video=true]
```

== Changelog ==

= 4.0.6 = 

* Fixed bug where date wouldn't "take" on first save

= 4.0.5 =

* Fix bug in sermon date component which prevented setting a date

= 4.0.4 =

* Fix missing Bible Scripture block in WordPress 6.1

= 4.0.3 =

Fix bug where sermons weren't able to be grouped by bible book

= 4.0.2 =
Test gitlab. No changes.

= 4.0.1 = 
Updated updater and moved source to gitlab.

= 4.0.0 =
Change the sermon edit screen to be a collection of blocks. This provides for more flexibility and better data management. 

= 3.5.2 =
Fixed a path issue.

= 3.5.0 =
Updated the ESV api to version 3. 
**Breaking Change!** You need to a new key for version 3.

= 3.4.0 =
Updated styles to better accommodate any content for a sermon.

= 3.3.0 =
Added option to latest sermon widget/shortcode to include any embedded video
that may be in the content area. 

= 3.2.0 =
Fixed permalinks to bible books.

= 3.1.4 =
Minor server-side update.

= 3.1.3 = 
Minor update to how updater works

= 3.1.2 = 
bugfix: If there's no sermon mp3 file, no link is displayed

= 3.1.1 =
Fixed paging through sermon taxonomies

= 3.1.0 = 
*Features*
Added a widget for the latest sermon.

*Bugfixes*
Fixed issue where searching via bible book returned no results.

= 3.0.2 =
A few minor bug fixes in setter methods.

= 3.0.1 =
Update sermon text immediately on sermon passage change in the editor

= 3.0.0 =
Started storing bible passages as free text. Added Occassions meta data.
Code refactoring.

= 2.2.0 =
Fixed display of sermon in new block editor to include sermon speaker & series.

= 2.1.1 =
Documentation updates.

= 2.1.0 =
Added an updater

= 2.0 =
Completed migration from in-theme code to a standalone plugin.