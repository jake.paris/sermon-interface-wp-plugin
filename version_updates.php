<?php
defined('ABSPATH') || die('not allowed here');

$stored_version = get_option('sermon_manager_interface_version', '0');

if( version_compare($stored_version, SERMON_MANAGER_INTERFACE_VERSION) > -1 )
	return;


switch( SERMON_MANAGER_INTERFACE_VERSION ) {

	case '3.2.1' :
	case '4.0.0' :
		add_action('init', function(){
			flush_rewrite_rules();
		});
		break;

	case '4.0.0' :
		delete_option('sermons_manager_settings');
		break;

}


update_option('sermon_manager_interface_version', SERMON_MANAGER_INTERFACE_VERSION);